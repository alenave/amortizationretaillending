require 'multi_parameter_attributes.rb'
class LoanDetail
  include Mongoid::Document
  field :loan_amount, type: Float
  field :rate_of_interest, type: Float
  field :tenure, type: Integer
  field :pre_payment, type: Float
  field :one_time_payment, type: Float, default: 0
  field :monthly_payment, type: Float, default: 0
  field :yearly_payment, type: Float, default: 0
  field :set_date_one_time_payment, type: DateTime, default: Time.now
  field :set_date_yearly_payment, type: DateTime, default: Time.now
  validates :loan_amount, :presence => true, :numericality => true
  validates :rate_of_interest, :presence => true, :numericality => true
  validates :tenure, :presence => true, :numericality => true, length: { minimum: 1, maximum: 2 }
  validates :one_time_payment, :numericality => true
  validates :monthly_payment, :numericality => true
  validates :yearly_payment, :numericality => true
end


