module Amortization
	def self.calculate(loan_detail)
		interest_rate_per_month = (loan_detail.rate_of_interest/12)/100
		number_of_months = loan_detail.tenure * 12
		details = []
		details_hash = {}
		total_interest_paid = 0

		emi = loan_detail.loan_amount * interest_rate_per_month * ((1 + interest_rate_per_month) ** number_of_months)/((1+interest_rate_per_month) ** (number_of_months) - 1)
		i = 1
		dateToday = Time.now

		if loan_detail.monthly_payment > 0.1
		  emi += loan_detail.monthly_payment
		end

		flag_one_time_payment = 0
		flag_yearly_payment = 0

		while loan_detail.loan_amount > 0.1
			dateToday = dateToday + 1.month
			monthly_interest_paid = loan_detail.loan_amount * interest_rate_per_month
			
			if ((loan_detail.set_date_one_time_payment).strftime("%b %Y").casecmp(dateToday.strftime("%b %Y")).zero?)
				flag_one_time_payment = loan_detail.one_time_payment
			end

			if ((loan_detail.set_date_yearly_payment).strftime("%b %Y").casecmp(dateToday.strftime("%b %Y")).zero?)
				flag_yearly_payment = loan_detail.yearly_payment
				loan_detail.set_date_yearly_payment += 1.year
			end

			
			if emi > (loan_detail.loan_amount + monthly_interest_paid)
				emi = (loan_detail.loan_amount + monthly_interest_paid)
			end

			principal_paid = emi - monthly_interest_paid + flag_one_time_payment + flag_yearly_payment
			new_balance = loan_detail.loan_amount - principal_paid

			if new_balance < 0.0
			  new_balance = 0.0
			end
			total_interest_paid = total_interest_paid + monthly_interest_paid
			details_hash.merge!({"number_of_months" => i, "emi" => (emi + flag_yearly_payment + flag_one_time_payment), "month_of_emi" => dateToday.strftime("%b %Y"), "loan_amount" => loan_detail.loan_amount, "monthly_interest_paid" => monthly_interest_paid, "total_interest_paid" => total_interest_paid, "principal_paid" => principal_paid, "new_balance" => new_balance})
			details.push(details_hash)
			details_hash = {}
			loan_detail.loan_amount = new_balance
			flag_one_time_payment = 0
			flag_yearly_payment = 0
			i += 1
		end

		return details
	end
end
