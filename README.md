# README #


### What is this repository for? ###

* to calculate the EMI for mortgage amount.
* if user is able to give extra payment in between the tenure of loan then      he/she can update.
* extra payment could be one-time, monthly, yearly.

### How do I get set up? ###

* clone the project
* from the project directory start rails server using 'rails s' on terminal
* start mongodb using 'mongod' on the terminal
* opne browser and type 'http://localhost:3000/'

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin